<?php

namespace SJRoyd\JPK\VAT\V3;

use SJRoyd\JPK\VAT\Helper;

class Schema
{
    use Helper\Schema;

    const NS = '{http://jpk.mf.gov.pl/wzor/2017/11/13/1113/}';
    const ETD = '{http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/}';
}
